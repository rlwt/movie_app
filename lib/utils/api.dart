import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:movie_app/models/movie_model.dart';

final _apiKey = '9c9576f8c2e86949a3220fcc32ae2fb6';
final _baseUrl = "http://api.themoviedb.org/3/movie";

Future<List<MovieModel>> fetchMovieList() async {
  final response = await http.get("$_baseUrl/popular?api_key=$_apiKey");
  try{
    if (response.statusCode == 200) {
      print("Inside 200 status code");
      Map<String, dynamic> res = json.decode(response.body);
      List<dynamic> list = res["results"];
      List<MovieModel> movieList = list.map((i)=> MovieModel.fromJson(i)).toList();
      return movieList;
//    return ItemModel.fromJson(json.decode(response.body));
    } else {
      print("Status code : ${response.statusCode}");
//    throw Exception('Failed to load movies list');
      return null;
    }
  }catch(err){
    print(err);
    return null;
  }
}
